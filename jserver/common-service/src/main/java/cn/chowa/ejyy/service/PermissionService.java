package cn.chowa.ejyy.service;

import cn.chowa.ejyy.common.Constants;
import cn.dev33.satoken.stp.StpInterface;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PermissionService implements StpInterface {

    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        return new ArrayList<>();
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        return List.of(Constants.RoleName.ANYONE, Constants.RoleName.YQFK,Constants.RoleName.WJDC);
    }
}
