package cn.chowa.ejyy.building;

import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.chowa.ejyy.dao.UserBuildingOperateLogRepository;
import cn.chowa.ejyy.dao.UserBuildingRepository;
import cn.chowa.ejyy.model.entity.UserBuilding;
import cn.chowa.ejyy.model.entity.UserBuildingOperateLog;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/pc/building")
public class binding {

    @Autowired
    private BuildingQuery buildingQuery;
    @Autowired
    private UserBuildingRepository userBuildingRepository;
    @Autowired
    private UserBuildingOperateLogRepository userBuildingOperateLogRepository;

    /**
     * 绑定
     */
    @SaCheckRole(Constants.RoleName.FCDA)
    @VerifyCommunity(true)
    @PostMapping("/binding")
    public void getBinding(long id, int building_id, int community_id) {
        Integer status = buildingQuery.getBindingStatus(id, building_id, community_id);
        if (status == null) {
            throw new CodeException(Constants.code.QUERY_ILLEFAL, "非法修改固定资产绑定关系");
        }

        if (status == Constants.status.UNBINDING_BUILDING) {
            throw new CodeException(Constants.code.STATUS_ERROR, "固定资产状态错误");
        }

        //设置绑定状态
        UserBuilding userBuilding = userBuildingRepository.findById(id).get();
        userBuilding.setStatus(Constants.status.BINDING_BUILDING);

        //保存操作日志
        userBuildingOperateLogRepository.save(
                UserBuildingOperateLog.builder()
                        .userBuildingId(id)
                        .propertyCompanyUserId(AuthUtil.getUid())
                        .status(Constants.status.BINDING_BUILDING)
                        .operateBy(Constants.operate_type.OPEARTE_BY_COMPANY)
                        .createdAt(System.currentTimeMillis())
                        .build()
        );

    }

}
