package cn.chowa.ejyy.option;


import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.utils.PhoneUtil;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.chowa.ejyy.dao.WechatMpUserRepository;
import cn.chowa.ejyy.model.entity.WechatMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cn.chowa.ejyy.common.Constants.building.*;
import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;
import static cn.chowa.ejyy.common.Constants.status.BINDING_BUILDING;
import static cn.chowa.ejyy.common.Constants.status.TRUE;

@RestController
@RequestMapping("/pc/option")
public class ower {

    @Autowired
    private WechatMpUserRepository wechatMpUserRepository;
    @Autowired
    private BuildingQuery buildingQuery;

    @PostMapping("/ower")
    public Map<?, ?> getOwer(@RequestBody RequestData data) {
        String phone = data.getStr("phone", true, "^1\\d{10}$");
        long community_id = data.getCommunityId();
        WechatMpUser userInfo = wechatMpUserRepository.findByPhoneAndIntact(phone, TRUE);
        if (userInfo == null) {
            throw new CodeException(QUERY_ILLEFAL, "未查询到业主信息");
        }
        List<ObjData> result = buildingQuery.getOwnerBuildings(userInfo.getId(), BINDING_BUILDING, community_id);
        if (result.size() == 0) {
            throw new CodeException(QUERY_ILLEFAL, "未查询到业主信息");
        }

        List<ObjData> houses = new ArrayList<>();
        List<ObjData> carports = new ArrayList<>();
        List<ObjData> warehouses = new ArrayList<>();
        List<ObjData> merchants = new ArrayList<>();
        List<ObjData> garages = new ArrayList<>();
        for (ObjData record : result) {
            int type = record.getInt("type");
            switch (type) {
                case HOUSE:
                    houses.add(record);
                    break;
                case CARPORT:
                    carports.add(record);
                    break;
                case WAREHOUSE:
                    warehouses.add(record);
                    break;
                case MERCHANT:
                    merchants.add(record);
                    break;
                case GARAGE:
                    garages.add(record);
                    break;
            }
        }

        phone = PhoneUtil.hide(userInfo.getPhone());

        return Map.of(
                "id", userInfo.getId(),
                "real_name", userInfo.getRealName(),
                "phone", phone,
                "avatar_url", userInfo.getAvatarUrl(),
                "houses", houses,
                "carports", carports,
                "warehouses", warehouses,
                "merchants", merchants,
                "garages", garages
        );
    }

}
