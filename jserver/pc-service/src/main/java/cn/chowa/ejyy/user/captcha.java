package cn.chowa.ejyy.user;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/pc/user")
public class captcha {

    /**
     * 获取验证码
     */
    @GetMapping("/captcha")
    public Map<String, Object> getCaptcha(HttpSession session) {
        LineCaptcha captcha = CaptchaUtil.createLineCaptcha(120, 36, 4, 10);
        session.setAttribute("captcha", captcha);
        captcha.createCode();
        return Map.of("img", "data:image/png;base64," + captcha.getImageBase64(),
                "expire", 50000
        );
    }

}
