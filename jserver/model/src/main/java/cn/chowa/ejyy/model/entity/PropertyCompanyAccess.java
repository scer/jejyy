package cn.chowa.ejyy.model.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "ejyy_property_company_access")
public class PropertyCompanyAccess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "name不能为空")
    private String name;

    @NotNull(message = "name不能为空")
    private String content;

}
