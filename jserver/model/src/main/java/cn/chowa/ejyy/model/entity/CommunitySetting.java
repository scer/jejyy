package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ejyy_community_setting")
public class CommunitySetting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("community_id")
    private long communityId;

    @JsonProperty("access_nfc")
    private int accessNfc;

    @JsonProperty("access_remote")
    private int accessRemote;

    @JsonProperty("access_qrcode")
    private int accessQrcode;

    @JsonProperty("carport_max_car")
    private int carportMaxCar;

    @JsonProperty("garage_max_car")
    private int garageMaxCar;

    /**
     * 装修保证金
     */
    @JsonProperty("fitment_pledge")
    private int fitmentPledge;

}
