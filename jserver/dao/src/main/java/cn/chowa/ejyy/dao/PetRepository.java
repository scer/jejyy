package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PetRepository extends JpaRepository<Pet, Long> {

    int countByCommunityId(long communityId);

    List<Pet> findByCommunityIdAndCreatedAtBetween(long communityId, long start, long end);

}
