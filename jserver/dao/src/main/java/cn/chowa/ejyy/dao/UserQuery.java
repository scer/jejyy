package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

@JqlQuery
public interface UserQuery {

    ObjData getUserInfo(int leaveOffice, String account);

}
